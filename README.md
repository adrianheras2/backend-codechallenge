# Code Challenge - Backend

Hola,

Antes de nada, agradecerte dedicar parte de tu tiempo a la realización de este pequeña prueba técnica.

# Enunciado

Imaginemos que un cliente solicita el envío de un pedido mediante una llamada a la API REST para almacenarlo en la base de datos.

El pedido debe contener:

- Nombre y apellidos del cliente
- Email (Único por cliente)
- Teléfono
- Dirección de entrega (solo puede existir una por pedido)
- Fecha de entrega
- Franja de hora seleccionada para la entrega (variable, pueden ser desde franjas de 1h hasta de 8h)

Una vez tenemos guardada la información del pedido, debe asignarse a un driver que tengamos dado de alta en el sistema de forma aleatoria.

Por otro lado, nuestros drivers mediante su aplicación, necesitan obtener el listado de tareas para completar en el día. Es necesario contar con un endpoint que reciba como parámetro el ID del driver y la fecha de los pedidos que queremos obtener y nos devuelva un JSON con el listado.

# PUNTOS DE REALIZACIÓN

## Arquitectura de aplicación en Laravel

Se trata de una API REST en Laravel donde:

- las llamadas a los endpoints se realizan mediante peticiones HTTP que se tratan con las librerías estándar de Laravel para ello

- Las peticiones serán a un endpoint determinado, con el método específico y los parámetros necesarios

- La base de datos se trata con el ORM Eloquent

- La autenticación se realiza con el paquete Laravel Passport, que es un completo servidor donde:

  - Se crea un cliente de la API
  - Se crea uno o varios usuarios que podrán usar la API, cada uno con un nombre de usuario y una contraseña
  - El login en la API se realizaría haciendo una llamada al endpoint oauth/token con los datos de usuario y de cliente de API,  con lo que se consigue si hay éxito un token de acceso de tipo Bearer
  - Con este token ya podemos acceder a ejecutar los endpoints de consumo de la API
  
- Se crean las rutas de los endpoints con el middleware "auth:api" y decimos que la lógica de negocio está en 'Http/Controllers/API/OrderController.php'

- Creamos la lógica de negocio en dicho fichero (Http/Controllers/API/OrderController.php)

- Los modelos los definimos en:

	- app\Order.php
	- app\Customer.php
	- app\Driver.php

- Las migraciones se definen en los ficheros:

	- database/migrations/2018_12_11_190233_create_drivers_table.php
	- database/migrations/2018_12_11_194656_create_orders_table.php
	- database/migrations/2018_12_12_170309_create_customers_table.php
	
- Se ha utilizado APIDOC para la documentación, se puede ver la implementación en cada método y documentación en el fichero índice "documentation/index.html"	

## Construir el modelo de datos en MYSQL con todas las entidades y relaciones

Modelo de datos en MYSQL con todas las entidades y relaciones (Modelo ER con cardinalidades):

```

--------------------                               ---------------------               
|     Customer     |                               |      Order        |
--------------------                               ---------------------                               ------------
| id               |                               | id                |                               |  Driver  |
| name             |   (1..1)             (0..n)   | delivery_date     |   (0..n)              (0..1)  |-----------
| surnames         |------------<has>--------------| strip_hour        |------------<has>--------------| id       |
| email            |                               | fk (customerr_id) |                               | name     |
| telephoneaddress |                               | fk (driver_id)    |                               | surnames |
--------------------                               ---------------------                               ------------
	

```

## Endpoints

Se han implementados las instrucciones APIDOC para la generación de la documentación de la API (end points), que puede consultarse 
en 'documentation/index.html'. El resumen se muestra a continuación:

- Endpoint para persistir el pedido en BD: POST /api/orders

    parámetros en el body de la request:
    
    - Siempre obligatorios:

        - email
        - delivery_date
        - strip_hour
    
    - Sólo obligatorios si el cliente no existe y hay que crearlo:
    
        - name
        - surnames
        - telephone
        - address
    
- Endpoint para mostrar los pedidos a entregar por los drivers:  GET  /api/orders

    parámetros en el body de la request:
    
    - driver_id
    - delivery_date

Nota: Si el cliente de un pedido ya existe en el sistema, respecto a los datos del cliente sólo hace falta definir el email (para que si un cliente hace un pedido por segunda vez, no tenga que meter sus datos ni tengan éstos que actualizarse con el consiguiente consumo de recursos innecesarios)    

Extra:

- Endpoint para mostrar un pedido determinado:         GET api/orders/:identificador_pedido
- Endpoint para borrar un pedido:				       DELETE /api/orders/:identificador_pedido

## Tests:

    Fichero tests\Feature\OrderControllerTest.php con varios casos de prueba para cada endpoint propuesto

# Evaluable
- Diseño modelado de datos
- API REST con sus endpoints
- Arquitectura de aplicación en Laravel
- Utilización del ORM
- Uso de buenas prácticas
- Patrones de diseño utilizados
- Optimización del performance

# Workflow
- Haz un fork de este repositorio.
- Resuelve el ejercicio.
- Comparte tu fork para la corrección (Reporter access)

Si tienes alguna duda, puedes contactar con nosotros en `tech@letsgoi.com`

Muchas gracias y suerte.