<?php

namespace App\Helpers\API;

class ApiResponseHelper
{

    /**
     * Enviar respuesta satisfactoria
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendSuccess($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        return response($response, 200)
            ->header('Content-Type', 'application/json; charset=UTF-8')
            ->header('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE,OPTIONS')
            ->header("Access-Control-Allow-Credentials", "true")
            ;
    }


    /**
     * Enviar respuesta de error
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}
