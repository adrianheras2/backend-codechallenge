<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'delivery_date', 'strip_hour'
    ];

    public function driver() {
        return $this->hasOne('App\Driver');
    }

    public function customer() {
        return $this->hasOne('App\Customer');
    }
}
