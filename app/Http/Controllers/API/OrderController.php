<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Order;
use App\Driver;
use App\Customer;
use Validator;
use ApiHttpResponse;

class OrderController extends Controller
{
    /**
     * Class attributes
     */
    private $orderAttrs = ['delivery_date', 'strip_hour'];
    private $customerAttrs = ['name', 'surnames', 'email', 'telephone', 'address'];


    /**
     * @api {get} /api/orders Mostrar el listado de todos los pedidos de un conductor y fecha determinados
     * @apiName ShowOrders
     * @apiGroup Api
     *
     * @apiHeader {String} Authorization Valor de Authorization.
     * @apiHeader {String} Accept Valor de Accept.
     *
     * @apiParam {String} driver_id Identificador del conductor.
     * @apiParam {String} delivery_date Fecha de entrega de la que se quieren los pedidos.
     *
     * @apiSuccess {Object[]} orders       Listado de pedidos.
     * @apiSuccess {Number}   order.id   Id del pedido.
     * @apiSuccess {String} order.name Nombre del cliente.
     * @apiSuccess {String} order.surnames Apellidos del cliente.
     * @apiSuccess {String} order.email Email del cliente.
     * @apiSuccess {String} order.telephone Teléfono del cliente.
     * @apiSuccess {String} order.address Dirección de entrega del cliente.
     * @apiSuccess {Date} order.delivery_date Fecha de entrega del pedido.
     * @apiSuccess {Integer} order.strip_hour Franja horaria de entrega.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {
        $input = $request->all();
        $rules = [
            'driver_id' => ['required', 'integer'],
            'delivery_date' => ['required', 'date']
        ];

        $validator = Validator::make($input, $rules);

        if($validator->fails()){
            return ApiHttpResponse::sendError('Error de validación', $validator->errors());
        }

        // Filtrar por driver y fecha de envío
        $orders = new Collection();
        if ( isset($input['driver_id']) && isset($input['delivery_date'])) {
            $orders = Order::where(['driver_id' => $input['driver_id'], 'delivery_date' => $input['delivery_date']])->get();
        }

        return ApiHttpResponse::sendSuccess($orders->toArray(), 'Pedidos obtenidos satisfactoriamente');
        return $this->sendSuccess($orders->toArray(), 'Pedidos obtenidos satisfactoriamente');
    }


    /**
     * @api {get} /api/orders/:id Mostrar un pedido específico
     * @apiName ShowOrder
     * @apiGroup Api
     *
     * @apiHeader {String} Authorization Valor de Authorization.
     * @apiHeader {String} Accept Valor de Accept.
     *
     * @apiSuccess {Number}   order.id   Id del pedido.
     * @apiSuccess {String} name Nombre del cliente.
     * @apiSuccess {String} surnames Apellidos del cliente.
     * @apiSuccess {String} email Email del cliente.
     * @apiSuccess {String} telephone Teléfono del cliente.
     * @apiSuccess {String} address Dirección de entrega del cliente.
     * @apiSuccess {Date} delivery_date Fecha de entrega del pedido.
     * @apiSuccess {Integer} strip_hour Franja horaria de entrega.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Se busca el pedido en cuestión
        $order = Order::find($id);

        // Si no se encuentra
        if (is_null($order)) {
            return ApiHttpResponse::sendError('Pedido no encontrado');
        }

        return ApiHttpResponse::sendSuccess($order->toArray(), 'Pedido obtenido satisfactoriamente');
    }


    /**
     * @api {post} /api/orders Crear un nuevo pedido en la base de datos
     * @apiName CreateOrder
     * @apiGroup Api
     *
     * @apiHeader {String} Authorization Valor de Authorization.
     * @apiHeader {String} Accept Valor de Accept.
     *
     * @apiParam {String} name Nombre del cliente.
     * @apiParam {String} surnames Apellidos del cliente.
     * @apiParam {String} email Email del cliente.
     * @apiParam {String} telephone Teléfono del cliente.
     * @apiParam {String} address Dirección de entrega del cliente.
     * @apiParam {Date} delivery_date Fecha de entrega del pedido.
     * @apiParam {Integer} strip_hour Franja horaria de entrega.
     *
     * @apiSuccess {String} name Nombre del cliente.
     * @apiSuccess {String} surnames Apellidos del cliente.
     * @apiSuccess {String} email Email del cliente.
     * @apiSuccess {String} telephone Teléfono del cliente.
     * @apiSuccess {String} address Dirección de entrega del cliente.
     * @apiSuccess {Date} delivery_date Fecha de entrega del pedido.
     * @apiSuccess {Integer} strip_hour Franja horaria de entrega.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $isCreationCustomer =false;
        $rules = [];

        // Comprobamos si existe ya un cliente con dicho email

        $customers = Customer::where('email', $input['email'])->get();
        $nCustomers = $customers->count();
        if  ($nCustomers > 1) {
            return ApiHttpResponse::sendError('Error de validación', 'No existe sólo un cliente con dicho email');
        } elseif ($nCustomers === 1) {
            $customer = $customers->first();

            // Si el email del cliente ya existe, sólo necesitamos los datos relativos al pedido
            $rules = [
                'email' => ['required', 'string', 'max:63'],
                'delivery_date' => ['required', 'date'],
                'strip_hour' => ['required', 'integer', 'between:1,8']
            ];
        } else {

            // Si el email del cliente no existe, sólo necesitamos los datos relativos al pedido y los del cliente
            $rules = [
                'name' => ['required', 'string', 'max:63'],
                'surnames' => ['required', 'string', 'max:127'],
                'email' => ['required', 'string', 'max:63'],
                'telephone' => ['required', 'string', 'max:63'],
                'address' => [ 'required', 'string', 'max:127'],
                'delivery_date' => ['required', 'date'],
                'strip_hour' => ['required', 'integer', 'between:1,8']
            ];
            $isCreationCustomer = true;
        }

        $validator = Validator::make($input, $rules);

        if($validator->fails()){
            return ApiHttpResponse::sendError('Error de validación', $validator->errors());
        }


        // Sólo creamos el cliente si no existe
        if ($isCreationCustomer) {
            $customer = new Customer;
            foreach ($this->customerAttrs as $attrName) {
                if (isset($input[$attrName])) {
                    $customer->$attrName = $input[$attrName];
                }
            }
            $customer->save();
        }

        // Si todo ok creamos el pedido
        $order = new Order;
        foreach ($this->orderAttrs as $attrName) {
            if (isset($input[$attrName])) {
                $order->$attrName = $input[$attrName];
            }
        }

        $customer->order()->save($order);
        $order->save();
        $driver = Driver::inRandomOrder()->first();
        $driver->order()->save($order);

        return ApiHttpResponse::sendSuccess($order->toArray(), 'Pedido creado satisfactoriamente');
    }


    /**
     * @api {delete} /api/orders/:id Borrar un pedido específico de la base de datos
     * @apiName DeleteOrder
     * @apiGroup Api
     *
     * @apiHeader {String} Authorization Valor de Authorization.
     * @apiHeader {String} Accept Valor de Accept.
     *
     * @apiSuccess {Number} id   Id del pedido.
     * @apiSuccess {String} name Nombre del cliente.
     * @apiSuccess {String} surnames Apellidos del cliente.
     * @apiSuccess {String} email Email del cliente.
     * @apiSuccess {String} telephone Teléfono del cliente.
     * @apiSuccess {String} address Dirección de entrega del cliente.
     * @apiSuccess {Date} delivery_date Fecha de entrega del pedido.
     * @apiSuccess {Integer} strip_hour Franja horaria de entrega.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        // Borramos el pedido en cuestión
        $order->delete();
        return ApiHttpResponse::sendSuccess($order->toArray(), 'Pedido eliminado satisfactoriamente');
    }
    
}