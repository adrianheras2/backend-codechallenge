<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use app\Order;
use app\Customer;
use app\Driver;

/**
 * PRE: se debe registrar un cliente de la API, un usuario asociado al mismo y loguearse con él para obtener un token de
 *      acceso de tipo Bearer
 *
 * Class OrderControllerTest
 * @package Tests\Feature
 */
class OrderControllerTest extends TestCase
{
    private $accessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI1NDM1YzZkZGU4NGM3MjlkNDFkOGRiOTQ2MjAwYTYyMzU3ZDAxNmQ5MzJkM2NmYjQ2ZDA1NGJlNjExZTQ3MGZhZTQzNDdhMGQ1Yjc3ZDQ2In0.eyJhdWQiOiIxIiwianRpIjoiMjU0MzVjNmRkZTg0YzcyOWQ0MWQ4ZGI5NDYyMDBhNjIzNTdkMDE2ZDkzMmQzY2ZiNDZkMDU0YmU2MTFlNDcwZmFlNDM0N2EwZDViNzdkNDYiLCJpYXQiOjE1NDQ0NTY4NDQsIm5iZiI6MTU0NDQ1Njg0NCwiZXhwIjoxNTc1OTkyODQ0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.UV3LWWcPLK6dHQ0W_F8_7v5j57GqaLjUW-dzYKsdIw8tfxX34f_9MZFSNFS4DaAMOSj6B57WIBIZZW-B6IEngCAIZh318qZoqcA_a8mxlL-jzEwSBH9Plv9NX9HTFopSGTmdL1MI7nlsIFkIS2matH0SAj8u9dEKWlO_sp9UaimyS8T2iPYEGIvzF2RvX41hdMvksU7OhMn4zSBbejqq8qGGV6Bd5QH5NtZviOmdAjPHHpfHqcVY3Nb6Lxd8gFnLVYwt8gOTSHTifGrPRWUx7dYVDHpDOJ3cjuhzUls3XrvugL9HwPCuUj5PfoDdTzhkb3oarV7_RXCv7H8gFXx1XOQi_SDSEHhXsjiCmXHMmonfBW4AioCHzKJ-ww4sa09_NlI2dwhPgZMgvOCjtCPkIIbCshhvhqn6ro7xJ5PTW8-cMeX_pvwYbnL0rsuxRyXnL0U_ADd8aJme8KWlsHIrFZlwWh3K40H0APWYfYv3KPJnzSlFnFBHkBfXOzzwUSwlJx1NDUbpcM58WxAO9mPdAKVO88T9knorXmkhUOUrFIOR7YgUUZ9I0oXZtUxaPXDD84-ko2-Mt8i5-6lp10zjLS_OZ1y8lkjkUdCvw6_2Dr1FOyott_4wnTbt6L56PL8JIPres5dxwA7pwnG8HT9gc5Exgj7ytm4a5VTCv9uxGig";
    private $orderAttrs = ['delivery_date', 'strip_hour'];
    private $customerAttrs = ['name', 'surnames', 'email', 'telephone', 'address'];

    /**
     * Se comprueban los distintos casos de creación de pedido (junto a la de cliente si no es existente)
     */
    public function testStoreOrder()
    {
        $newDatas = [
            [
                'delivery_date' => '2018-08-02',
                'name' => 'Julia',
                'surnames' => 'Tomczak Pérez',
                'email' => 'julia30@company.com',
                'telephone' => '666555444',
                'address' => 'C. Mayor, 32. 22222 Alcobendas (Madrid)',
                'strip_hour' => '2',

            ],
            // Pedido del mismo cliente (existente) pero de nuevo con todos sus datos (se trata la creación del pedido
            // pero no se deben actualizar los datos del cliente)
            [
                'delivery_date' => '2018-08-02',
                'name' => 'Julia',
                'surnames' => 'Tomczak Pérez',
                'email' => 'julia30@company.com',
                'telephone' => '666555444',
                'address' => 'C. Mayor, 32. 22222 Alcobendas (Madrid)',
                'strip_hour' => '3',
            ],
            // Pedido del mismo cliente pero no se definen los datos del mismo (es existente)
            [
                'delivery_date' => '2018-08-02',
                'email' => 'julia30@company.com',
                'strip_hour' => '4'
            ],
            // Pedido de nuevo cliente (PRE: no hay ningún cliente juan\d+@company.com creado fuera de esta prueba)
            [
                'delivery_date' => '2018-08-02',
                'name' => 'Juan',
                'surnames' => 'Rodríguez Martín',
                'email' => 'juan' . time() . '@company.com',
                'telephone' => '666555333',
                'address' => 'C. Real, 32. 22223 Algete (Madrid)',
                'strip_hour' => '5',
            ],

        ];

        foreach ($newDatas as $newData) {

            $headers = [
                "Authorization" => $this->accessToken,
                "Accept" => "application/json"
            ];

            // se crea el pedido (junto con el cliente si no existe)
            $response = $this->withHeaders(
                $headers
            )->json('POST', 'http://apipedidos/api/orders', $newData);

            $response
                ->assertStatus(200)// Success
                ->assertJsonStructure(['success', 'data', 'message']);

            // Se comprueban los datos persistidos de la creación del pedido (Order)
            $jsonResponse = $response->decodeResponseJson()['data'];
            $orderId = $jsonResponse['id'];
            $orderInstance = Order::find($orderId);
            $aOrderInstance = $orderInstance->toArray();

            foreach ($this->orderAttrs as $orderAttr) {
                $this->assertEquals($newData[$orderAttr], $aOrderInstance[$orderAttr]);
            }

            // Se comprueban los datos persistidos de la creación del cliente (si no existía el cliente)
            $customerId = $jsonResponse['customer_id'];
            $customerOrders = Order::where('customer_id', $customerId)->get();
            $nCustomerOrders = $customerOrders->count();
            if ($nCustomerOrders === 1) {
                $customerInstance = Customer::find($customerId);
                $aCustomerInstance = $customerInstance->toArray();
                foreach ($this->customerAttrs as $customerAttr) {
                    $this->assertEquals($newData[$customerAttr], $aCustomerInstance[$customerAttr]);
                }
            }
        }
    }


    /**
     * Se comprueba el listado de pedidos existente para cada conductor en una fecha determinada
     */
    public function testIndexOrders()
    {
        $headers = [
            "Authorization" => $this->accessToken,
            "Accept" => "application/json"
        ];

        foreach (Driver::all() as $driver){
            $response = $this->withHeaders(
                $headers
            )->json('GET', 'http://apipedidos/api/orders', ['driver_id' => 1, 'delivery_date' => '2018-08-02']);

            $response
                ->assertStatus(200) // Success
                ->assertJsonStructure(['success', 'data', 'message'])
            ;
        }

    }

}
